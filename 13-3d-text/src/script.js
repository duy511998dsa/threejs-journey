import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'dat.gui'
import { AxesHelper } from 'three'



/**
 * Base
 */
// Debug
const gui = new dat.GUI()

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

/**
 * Axes helper
 */
const axesHelper = new THREE.AxesHelper()

// scene.add(axesHelper);
/**
 * Textures
 */
const textureLoader = new THREE.TextureLoader()
const matcapTexture = textureLoader.load('/textures/matcaps/1.png')

/**
 * Fonts
 */

const fontLoader = new THREE.FontLoader()



fontLoader.load('/fonts/helvetiker_regular.typeface.json', (font) => {
    const textParams = {
        font: font,
        size: 0.5,
        height: 0.2,
        curveSegments: 12,
        bevelEnabled: true,
        bevelThickness: 0.03,
        bevelSize: 0.02,
        bevelOffset: 0,
        bevelSegments: 5
    }

    const textGeometry = new THREE.TextBufferGeometry('Duy Nguyen', textParams);
    textGeometry.computeBoundingBox();
    
    // textGeometry.translate(
    //     - (textGeometry.boundingBox.max.x - textParams.bevelSize) * 0.5,
    //     - (textGeometry.boundingBox.max.y - textParams.bevelSize) * 0.5,
    //     - (textGeometry.boundingBox.max.z - textParams.bevelThickness) * 0.5
    // )
    
    textGeometry.center();

    console.log(textGeometry.boundingBox);
    const material = new THREE.MeshMatcapMaterial({
        matcap: matcapTexture
    });
    const text = new THREE.Mesh(textGeometry, material);
    scene.add(text);

    const donutGeometry = new THREE.TorusBufferGeometry(0.3, 0.2, 20, 45);
    console.time('donuts')
    for(let i = 0; i < 250; i++) {
        const donut = new THREE.Mesh(donutGeometry, material);
        // console.log('hello');
        // donut.position.set(
        //     (Math.random() - 0.5) * 10,
        //     (Math.random() - 0.5) * 10,
        //     (Math.random() - 0.5) * 10,
        // )
        
        donut.position.x = (Math.random() - 0.5) * 10;
        donut.position.y = (Math.random() - 0.5) * 10;
        donut.position.z = (Math.random() - 0.5) * 10;
        
        // donut.rotation.set(
            // Math.random() * Math.PI,
            // Math.random() * Math.PI,
        // )

        donut.rotation.x = Math.random() * Math.PI;
        donut.rotation.y = Math.random() * Math.PI;

        const scale = Math.random()
        donut.scale.set(
            scale,
            scale,
            scale,
        )
        scene.add(donut);
    }
    console.timeEnd('donuts')

});



/**
 * Object
 */
const cube = new THREE.Mesh(
    new THREE.BoxBufferGeometry(1, 1, 1),
    new THREE.MeshBasicMaterial()
)

// scene.add(cube)

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.x = 1
camera.position.y = 1
camera.position.z = 2
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

/**
 * Animate
 */
const clock = new THREE.Clock()

const tick = () =>
{
    const elapsedTime = clock.getElapsedTime()

    // Update controls
    controls.update()

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()